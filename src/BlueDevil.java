import java.util.ArrayList;
import java.util.Scanner;

class BlueDevil extends Person {
  public String major;
  public String title;
  public Experience education_exps;
  public Experience working_exps;

  public BlueDevil() {
    this.major = "NONE";
    this.title = "NONE";
  }

  public BlueDevil(boolean isMale, String name, String nationality,
                   String hobbies, Experience general_education_exps,
                   Experience general_working_exps, String major, String title,
                   Experience education_exps, Experience working_exps) {
    super(isMale, name, nationality, hobbies, general_education_exps,
          general_working_exps);
    this.major = major;
    this.title = title;
    this.education_exps = education_exps;
    this.working_exps = working_exps;
  }

  public void print_message() {
    super.print_general_message();
    System.out.println(major);
    System.out.println(title);
    System.out.println(education_exps.experience);
    System.out.println(working_exps.experience);
  }
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    ArrayList<BlueDevil> person_list = new ArrayList<BlueDevil>(0);
    while (true) {
      System.out.println("Enter ADD to add a new person.");
      System.out.println("Enter FIND to find a person.");
      System.out.println("Enter END to quit.");
      String mode = scan.nextLine();
      if (mode.equals("FIND")) {
        System.out.println("Please enter the NAME you want to search?");
        String search_name = scan.nextLine();
        boolean isFound = false;
        for (int i = 0; i < person_list.size(); ++i) {
          if (person_list.get(i).name.equals(search_name)) {
            person_list.get(i).print_message();
            isFound = true;
            break;
          }
        }
        if (isFound == false) {
          System.out.println("Name doesn't exist.");
        }
        // print message
      } else if (mode.equals("ADD")) {
        System.out.println("Are you a male or a female?(F/M)");
        boolean isMale = scan.nextLine() == "M";
        System.out.println("Please enter the name of new person:");
        String name = scan.nextLine();
        System.out.println("Please enter the nationality of new person:");
        String nationality = scan.nextLine();
        System.out.println("Please enter the hobby of new person:");
        String hobby = scan.nextLine();
        System.out.println(
            "Please enter the working experience of new person(outside Duke):");
        String working_exp = scan.nextLine();
        Experience working_exps = new Experience(working_exp);
        System.out.println(
            "Please enter the education experience of new person(outside Duke):");
        String education_exp = scan.nextLine();
        Experience education_exps = new Experience(education_exp);
        System.out.println("Are you a BlueDevil?(Y/N)");
        boolean ans = scan.nextLine().equals("Y");
        String major = "";
        String title = "";
        Experience duke_working_exps = new Experience();
        Experience duke_education_exps = new Experience();
        if (ans) {
          System.out.println("Please enter the major of new person:");
          major = scan.nextLine();
          System.out.println("Please enter the title of new person:");
          title = scan.nextLine();
          System.out.println(
              "Please enter the working experience of new person(inside Duke):");
          String duke_working_exp = scan.nextLine();
          duke_working_exps = new Experience(duke_working_exp);
          System.out.println(
              "Please enter the education experience of new person(inside Duke):");
          String duke_education_exp = scan.nextLine();
          duke_education_exps = new Experience(duke_education_exp);
        }
        BlueDevil newperson = new BlueDevil(
            isMale, name, nationality, hobby, education_exps, working_exps,
            major, title, duke_education_exps, duke_working_exps);
        person_list.add(newperson);
      } else if (mode.equals("END")) {
        break;
      } else {
        System.out.println("Please enter a valid command. (ADD / FIND / END)");
      }
    }
    scan.close();
  }
}

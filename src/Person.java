//import java.util.*;

class Experience {
	public String experience;
	
	public Experience() {
		experience = new String("");
	}
	public Experience(String new_exp){
		experience = new_exp;
	}
}

class Person {
	public boolean isMale;
	public String name;
	public String nationality;
	public String hobbies;
	public Experience education_exps;
	public Experience working_exps;
	
	public Person() {
		this.isMale = false;
		this.name = "NONE";
		this.nationality = "NONE";
		this.hobbies = new String("");
		this.education_exps = new Experience();
		this.working_exps = new Experience();
	}
	
	public Person(boolean isMale, String name, String nationality, String hobbies,
				Experience education_exps, Experience working_exps) {
		this.isMale = isMale;
		this.name = name;
		this.nationality = nationality;
		this.hobbies = hobbies;
		this.education_exps = education_exps;
		this.working_exps = working_exps;
	}
	public void print_general_message() {
		System.out.println(isMale);
		System.out.println(name);
		System.out.println(nationality);
		System.out.println(hobbies);
		System.out.println(education_exps.experience);
		System.out.println(working_exps.experience);
		System.out.println(isMale);
	}
}


